const { Server } = require('socket.io')

const io = new Server(5001, {
    serveClient: false,
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false,
    cors: {
        origin: 'http://localhost:3000',
        methods: ['GET', 'POST'],
    },
})

io.on('connection', function (socket) {
    console.log('User Connection')

    socket.on('joinRoom', ({ name, room }) => {
        socket.join(room)
        console.log('User Join Room', name, room)

        socket.broadcast.emit('needSupport', { author: name })

        socket.broadcast.to(room).emit('message', { author: 'Bot', message: `${name} has joined the chat` })
    })

    socket.on('joinBooking', ({ name, room, isRoot }) => {
        socket.join(room)
        console.log('User Join Booking', name, room)

        socket.broadcast.to(room).emit('booking', {
            author: 'Bot',
            message: isRoot ? 'Created booking room...' : `${name} has joined booking room`,
        })
    })

    //Message listening
    socket.on('message', ({ author, room, message }) => {
        console.log('[server](message): %s', author, message, room)
        io.to(room).emit('message', { author, message })
    })

    socket.on('booking', ({ name, seat, room }) => {
        console.log('[server](booking): %s', name, seat)
        io.to(room).emit('booking', {
            author: name,
            message: `${name} has booked ${seat}`,
            seat,
        })
    })

    socket.on('disconnect', () => {
        console.log('User Left')
    })
})

console.log('Socket app is running on port', 5001)
