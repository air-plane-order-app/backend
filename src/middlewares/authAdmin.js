const Users = require('../apis/models/user.model')

const authAdmin = async (req, res, next) => {
    try {
        const user = await Users.findById(req.user.sub).populate('role')

        if (user.role.roleName !== 'admin') return res.status(500).json({ msg: 'Admin resources access denied.' })

        next()
    } catch (err) {
        return res.status(500).json({ msg: err.message })
    }
}

module.exports = authAdmin
