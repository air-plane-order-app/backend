const jwt = require('jsonwebtoken')
const { ACCESS_TOKEN_SECRET } = process.env

const auth = (req, res, next) => {
    try {
        const token = req.header('authorization').split(' ')[1]
        if (!token) return res.status(400).json({ msg: 'Invalid Authentication.' })

        jwt.verify(token, ACCESS_TOKEN_SECRET, (err, user) => {
            console.log('err', err)
            if (err) return res.status(401).json({ msg: 'Invalid Authentication.' })

            req.user = user
            next()
        })
    } catch (err) {
        return res.status(500).json({ msg: err.message })
    }
}

module.exports = auth
