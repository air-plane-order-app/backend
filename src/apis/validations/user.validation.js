const Joi = require('joi')

const { password } = require('./customize.validation')

const loginSchema = {
    body: Joi.object().keys({
        email: Joi.string().required(),
        password: Joi.string().required(),
    }),
}

const logoutSchema = {
    body: Joi.object().keys({
        accessToken: Joi.string().required(),
        refreshToken: Joi.string().required(),
    }),
}

const registerSchema = {
    body: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        password: Joi.string().required().custom(password),
        isVerified: Joi.boolean().allow(null),
        role: Joi.object().allow(null),
    }),
}

module.exports = {
    loginSchema,
    logoutSchema,
    registerSchema,
}
