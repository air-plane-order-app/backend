const Joi = require('joi')

const { password } = require('./customize.validation')

const getAllSchema = {
    body: Joi.object().keys({
        departureFromLocation: Joi.string().required(),
        departureToLocation: Joi.string().required(),
        departureStartTime: Joi.string().required(),
        arrivalStartTime: Joi.string().required(),
        type: Joi.string().required(),
        totalSeat: Joi.number().required(),
    }),
}

const createSchema = {
    body: Joi.object().keys({
        departureFromLocation: Joi.string().required(),
        departureToLocation: Joi.string().required(),
        departureStartTime: Joi.string().required(),
        departureEndTime: Joi.string().required(),
        flightStartTime: Joi.string().required(),
        flightEndTime: Joi.string().required(),
        airplaneName: Joi.string().allow(null).allow(''),
        arrivalFromLocation: Joi.string().allow(null).allow(''),
        arrivalToLocation: Joi.string().allow(null).allow(''),
        arrivalStartTime: Joi.string().allow(null).allow(''),
        arrivalEndTime: Joi.string().allow(null).allow(''),
        flightArrivalStartTime: Joi.string().allow(null).allow(''),
        flightArrivalEndTime: Joi.string().allow(null).allow(''),
        availableSeat: Joi.number().required(),
        totalSeat: Joi.number().required(),
        ecoClass: Joi.number().required(),
        businessClass: Joi.number().required(),
        firstClass: Joi.number().required(),
        type: Joi.string().required(),
    }),
}

const deleteSchema = {
    body: Joi.object().keys({
        id: Joi.string().required(),
    }),
}

const logoutSchema = {
    body: Joi.object().keys({
        accessToken: Joi.string().required(),
        refreshToken: Joi.string().required(),
    }),
}

const registerSchema = {
    body: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        password: Joi.string().required().custom(password),
    }),
}

module.exports = {
    getAllSchema,
    logoutSchema,
    registerSchema,
    createSchema,
    deleteSchema,
}
