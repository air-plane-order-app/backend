const router = require('express').Router()
const validate = require('../../middlewares/validate')

const { flightController } = require('../controllers')
const { flightValidation } = require('../validations')

const auth = require('../../middlewares/auth')
const authAdmin = require('../../middlewares/authAdmin')
const { authenticate } = require('passport')

router.post('/get-all', auth, validate(flightValidation.getAllSchema), flightController.getAll)

router.post('/create', auth, validate(flightValidation.createSchema), flightController.createFlight)

router.post('/get-all-paged', auth, flightController.getAllPagination)

router.get('/get-all-no-paged', auth, flightController.getAllNoPagination)

router.post('/delete', auth, validate(flightValidation.deleteSchema), flightController.deleteFlight)

router.post('/pay-pal', auth, flightController.payFlight)

router.get('/success', flightController.paypalSuccess)

router.post('/tickets', auth, flightController.getTickets)

module.exports = router
