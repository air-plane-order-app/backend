const router = require('express').Router()
const messageController = require('../controllers/message.controller')
const auth = require('../../middlewares/auth')

router.post('/create-message', messageController.newMessage)

router.get('/get-message', messageController.getMessage)

router.post('/delete-message', messageController.deleteMessage)

module.exports = router
