const express = require('express')

// const authRoute = require('./v1/auth.route')
const userRoute = require('./user.route')
const flightRoute = require('./flight.route')
const messageRoute = require('./message.route')

const router = express.Router()

const defaultRoutes = [
    {
        path: '/user',
        route: userRoute,
    },
    {
        path: '/flight',
        route: flightRoute,
    },
    {
        path: '/message',
        route: messageRoute,
    },
]

defaultRoutes.forEach((route) => {
    router.use(route.path, route.route)
})

module.exports = router
