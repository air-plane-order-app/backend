const router = require('express').Router()
const userController = require('../controllers/user.controller')

const auth = require('../../middlewares/auth')
const authAdmin = require('../../middlewares/authAdmin')

const { userValidation } = require('../validations')

const validate = require('../../middlewares/validate')

router.post('/register', validate(userValidation.registerSchema), userController.register)

router.post('/activation', userController.activateEmail)

router.post('/login', userController.login)

router.post('/refresh-token', userController.refreshToken)

router.post('/forgot', userController.forgotPassword)

router.post('/reset', userController.resetPassword)

router.get('/info', auth, userController.getUserInfo)

router.get('/all-info', auth, userController.getAllUsersInfo)

router.get('/logout', userController.logout)

router.get('/info/:id', auth, userController.getUserInfoById)

router.patch('/update-role/:id', auth, authAdmin, userController.updateUsersRole)

router.patch('/update-user/:id', auth, userController.updateUser)

router.delete('/delete/:id', auth, authAdmin, userController.deleteUser)

// Social Login
router.post('/google-login', userController.googleLogin)

router.post('/facebook-login', userController.facebookLogin)

module.exports = router
