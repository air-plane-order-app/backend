const catchAsync = require('../../utils/catch-async')
const httpStatus = require('http-status')
const paypal = require('paypal-rest-sdk')

const { flightService } = require('../services')

paypal.configure({
    mode: 'sandbox', //sandbox or live
    client_id: 'AfuS7PgUp_E-1L2jORgJD8SNYQRkrLlCNtfKUuQlFXBd9Zm3CDIlSuRu9VsBhkdsPoqHMtxmw8146B1l',
    client_secret: 'EGbZbZywOIOoqyU6lfFpul2uUNsDmA96Za0Pl2Absb9L-LhN4Dyzc77cPq2Z6BTVM0VrE0rmNUBpUBjs',
})

let currentFlight = {
    id: null,
    currentPrice: 0,
    user: null,
    totalPrice: null,
}

const getAll = catchAsync(async (req, res) => {
    const flight = await flightService.getAll(req.body)

    res.status(httpStatus.OK).send({ flight })
})
const getAllPagination = catchAsync(async (req, res) => {
    const flight = await flightService.getAllPagination(req.body)

    res.status(httpStatus.OK).send({ flight })
})
const getAllNoPagination = catchAsync(async (req, res) => {
    const flight = await flightService.getAllNoPagination()

    res.status(httpStatus.OK).send({ flight })
})

const createFlight = catchAsync(async (req, res) => {
    const flight = await flightService.createFlight(req.body)

    res.status(httpStatus.OK).send({ flight })
})

const deleteFlight = catchAsync(async (req, res) => {
    const flight = await flightService.deleteFlight(req.body)

    res.status(httpStatus.OK).send({ flight })
})

const payFlight = catchAsync(async (req, res) => {
    const {
        id,
        departureFromLocation,
        departureToLocation,
        departureStartTime,
        arrivalStartTime,
        classTicket,
        totalSeat,
        seat,
        price,
    } = req.body

    const dollar = Math.round((price * totalSeat + (price * 10) / 100) / 22910)

    const create_payment_json = {
        intent: 'sale',
        payer: {
            payment_method: 'paypal',
        },
        redirect_urls: {
            return_url: 'http://localhost:5000/api/flight/success',
            cancel_url: 'http://localhost:3000/cancel',
        },
        transactions: [
            {
                item_list: {
                    items: [
                        {
                            name: `NK-UTE Flight Ticket: ${departureFromLocation} - ${departureToLocation}`,
                            price: `${dollar}.00`,
                            currency: 'USD',
                            quantity: 1,
                        },
                    ],
                },
                amount: {
                    currency: 'USD',
                    total: `${dollar}.00`,
                },
                description: `NK-UTE Flight Ticket: ${departureFromLocation} - ${departureToLocation}, ${departureStartTime} - ${arrivalStartTime}, Class: ${classTicket}`,
            },
        ],
    }

    currentFlight = {
        id: id,
        currentPrice: dollar,
        user: req?.user?.sub,
        totalPrice: dollar * 22910,
        seat: seat,
        totalSeat: totalSeat,
    }

    await paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            throw error
        } else {
            for (let i = 0; i < payment.links.length; i++) {
                if (payment.links[i].rel === 'approval_url') {
                    res.status(httpStatus.OK).send(payment.links[i].href)
                }
            }
        }
    })
})

const paypalSuccess = catchAsync(async (req, res) => {
    const payerId = req.query.PayerID
    const paymentId = req.query.paymentId

    const execute_payment_json = {
        payer_id: payerId,
        transactions: [
            {
                amount: {
                    currency: 'USD',
                    total: `${currentFlight.currentPrice}.00`,
                },
            },
        ],
    }
    await paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response)
            throw error
        } else {
            flightService.createTicket({
                id: currentFlight.user,
                ticketId: currentFlight.id,
                totalPrice: currentFlight.totalPrice,
                seat: currentFlight.seat,
            })

            flightService.updateFlight({ id: currentFlight.id, available: currentFlight.totalSeat })
            res.redirect('http://localhost:3000/my-ticket')
        }
    })
})

const getTickets = catchAsync(async (req, res) => {
    const response = await flightService.getTickets(req.user.sub)
    if (response) return res.status(httpStatus.OK).send({ flight: response })

    return res.status(httpStatus.NOT_FOUND).send({ flight: null })
})

module.exports = {
    getAll,
    getAllPagination,
    getAllNoPagination,
    createFlight,
    deleteFlight,
    payFlight,
    paypalSuccess,
    getTickets,
}
