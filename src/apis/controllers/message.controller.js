const catchAsync = require('../../utils/catch-async')
const httpStatus = require('http-status')
const { messageService } = require('../services')

const newMessage = catchAsync(async (req, res) => {
    const message = await messageService.createMessage(req.body)

    res.status(httpStatus.OK).send({ message })
})

const getMessage = catchAsync(async (req, res) => {
    const message = await messageService.getMessage()

    res.status(httpStatus.OK).send({ message })
})

const deleteMessage = catchAsync(async (req, res) => {
    const message = await messageService.deleteMessage(req.body)

    res.status(httpStatus.OK).send({ result: 'OK' })
})

module.exports = {
    newMessage,
    getMessage,
    deleteMessage,
}
