const User = require('../models/user.model')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const sendMail = require('../services/email.service')

const { google } = require('googleapis')
const { OAuth2 } = google.auth
const fetch = require('node-fetch')
const { tokenService, userService } = require('../services')
const { policytroubleshooter } = require('googleapis/build/src/apis/policytroubleshooter')

const client = new OAuth2(process.env.MAILING_SERVICE_CLIENT_ID)

const { CLIENT_URL } = process.env

const userController = {
    register: async (req, res) => {
        try {
            const { email } = req.body

            const user = await userService.createUser(req.body)
            const token = await tokenService.generateAuthTokens(user)

            const url = `${CLIENT_URL}/user/activate/${token?.access?.token}`
            console.log(url, email)
            sendMail(email, url, 'Verify your email address')

            res.json({ msg: 'Success! Please activate email' })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    activateEmail: async (req, res) => {
        try {
            const { activation_token } = req.body
            const user = jwt.verify(activation_token, process.env.ACCESS_TOKEN_SECRET)

            const { sub, exp } = user
            console.log('expire', exp, new Date().getTime())

            if (exp < moment().unix()) {
                return res.status(401).json({ msg: 'Token has expired!' })
            }

            await User.findOneAndUpdate(
                { _id: sub },
                {
                    isVerified: true,
                }
            )

            res.json({ msg: 'Account has been activated!' })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    login: async (req, res) => {
        try {
            const { email, password } = req.body

            const user = await userService.loginUserWithEmailAndPassword(email, password)
            const tokens = await tokenService.generateAuthTokens(user)

            res.send({ msg: 'Login success!', user, tokens })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    refreshToken: async (req, res) => {
        try {
            const { refreshToken } = req.body
            if (!refreshToken) return res.status(401).json({ msg: 'Please login now!' })

            const oldRefresh = await tokenService.getTokenByRefresh(refreshToken)
            const oldRefreshToken = jwt.verify(oldRefresh?.token, process.env.ACCESS_TOKEN_SECRET)

            if (!oldRefreshToken || oldRefreshToken.exp < moment().unix()) {
                res.status(401).json({ msg: 'Please login now!' })
            }

            const user = await userService.getUserById(oldRefreshToken.sub)
            const tokens = await tokenService.generateAuthTokens(user)

            res.send({ user, tokens })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    forgotPassword: async (req, res) => {
        try {
            const { email } = req.body

            const user = await userService.getUserByEmail(email)
            if (!user) return res.status(400).json({ msg: 'This email does not exist.' })

            const tokens = await tokenService.generateAuthTokens(user)

            const url = `${CLIENT_URL}/user/reset/${tokens.access.token}`
            sendMail(email, url, 'Reset your password')

            res.json({ msg: 'Re-send the password, please check your email.' })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    resetPassword: async (req, res) => {
        try {
            const { password, id } = req.body
            const result = await userService.resetPassword(password, id)

            res.json({ msg: 'Password successfully changed!' })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    getUserInfo: async (req, res) => {
        try {
            const user = await userService.getUserById(req.user.sub)
            res.json(user)
        } catch (err) {}
    },

    getAllUsersInfo: async (req, res) => {
        try {
            const users = await userService.getAllUsers()
            res.json(users)
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    getUserInfoById: async (req, res) => {
        try {
            const user = await userService.getUserById(req.params.id)
            res.json(user)
        } catch (err) {}
    },
    logout: async (req, res) => {
        try {
            await tokenService.logout(req.body.refreshToken)
            return res.json({ msg: 'Logged out.' })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    updateUser: async (req, res) => {
        try {
            const { name, phone, address, idNumber, birthDate, avatar } = req.body.params
            const user = await User.findOneAndUpdate(
                { _id: req.params.id },
                {
                    name,
                    phone,
                    address,
                    idNumber,
                    birthDate,
                    avatar,
                }
            ).populate('role')

            res.json({ msg: 'Update Success!', user })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    updateUsersRole: async (req, res) => {
        try {
            const { role } = req.body

            await User.findOneAndUpdate(
                { _id: req.params.id },
                {
                    role,
                }
            )

            res.json({ msg: 'Update Success!' })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    deleteUser: async (req, res) => {
        try {
            await userService.deleteUser(req.params.id)

            res.json({ msg: 'Deleted Success!' })
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
    googleLogin: async (req, res) => {
        try {
            const { tokenId } = req.body

            const verify = await client.verifyIdToken({ idToken: tokenId, audience: process.env.MAILING_SERVICE_CLIENT_ID })
            const { email_verified, email, name, picture } = verify.payload
            if (!email_verified) return res.status(400).json({ msg: 'Email verification failed.' })

            const isExist = await User.isEmailTaken(email)

            if (isExist) {
                const user = await userService.getUserByEmail(email)
                const tokens = await tokenService.generateAuthTokens(user)

                res.json({ msg: 'Login success!', tokens })
            } else {
                const data = {
                    name,
                    email,
                    password: 'test123',
                    avatar: picture?.data?.url,
                }

                const user = userService.createUser(data)
                const tokens = await tokenService.generateAuthTokens(user)

                res.json({ msg: 'Login success!', tokens })
            }
        } catch (err) {
            return res.status(500).json({ msg: err?.message })
        }
    },
    facebookLogin: async (req, res) => {
        try {
            const { accessToken, userID } = req.body

            const URL = `https://graph.facebook.com/v2.9/${userID}/?fields=id,name,email,picture&access_token=${accessToken}`
            const data = await fetch(URL)
                .then((res) => res.json())
                .then((res) => {
                    return res
                })

            const { email, name, picture } = data
            const isExist = await User.isEmailTaken(email)

            if (isExist) {
                const user = await userService.getUserByEmail(email)
                const tokens = await tokenService.generateAuthTokens(user)

                res.json({ msg: 'Login success!', tokens })
            } else {
                const data = {
                    name,
                    email,
                    password: 'test123',
                    avatar: picture.data.url,
                }

                const user = userService.createUser(data)
                const tokens = await tokenService.generateAuthTokens(user)

                res.json({ msg: 'Login success!', tokens })
            }
        } catch (err) {
            return res.status(500).json({ msg: err.message })
        }
    },
}

module.exports = userController
