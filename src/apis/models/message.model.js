const mongoose = require('mongoose')
const Schema = mongoose.Schema

const messageSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
        },
        room: {
            type: String,
            trim: true,
        },
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('Message', messageSchema)
