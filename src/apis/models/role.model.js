const mongoose = require('mongoose')

const roleSchema = new mongoose.Schema(
    {
        roleName: {
            type: String,
            trim: true,
        },
    },
    {
        timestamps: true,
    }
)

roleSchema.statics.isRoleExist = async function (name) {
    const role = await this.findOne({ roleName: name })
    return !!role
}

module.exports = mongoose.model('Role', roleSchema)
