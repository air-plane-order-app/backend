const mongoose = require('mongoose')
const Schema = mongoose.Schema

const flightSchema = new mongoose.Schema(
    {
        airplaneName: {
            type: String,
            trim: true,
        },
        departureFromLocation: {
            type: String,
            trim: true,
        },
        departureToLocation: {
            type: String,
            trim: true,
        },
        departureStartTime: {
            type: String,
            trim: true,
        },
        flightStartTime: {
            type: String,
            trim: true,
        },
        flightEndTime: {
            type: String,
            trim: true,
        },
        flightArrivalStartTime: {
            type: String,
            trim: true,
        },
        flightArrivalEndTime: {
            type: String,
            trim: true,
        },
        departureEndTime: {
            type: String,
            trim: true,
        },
        arrivalFromLocation: {
            type: String,
            trim: true,
        },
        arrivalToLocation: {
            type: String,
            trim: true,
        },
        arrivalStartTime: {
            type: String,
            trim: true,
        },
        arrivalEndTime: {
            type: String,
            trim: true,
        },
        totalSeat: {
            type: Number,
            trim: true,
        },
        availableSeat: {
            type: Number,
            trim: true,
        },
        ecoClass: {
            type: Number,
            trim: true,
        },
        businessClass: {
            type: Number,
            trim: true,
        },
        firstClass: {
            type: Number,
            trim: true,
        },
        type: {
            type: String,
            trim: true,
        },
        status: {
            type: String,
            trim: true,
        },
        isDeleted: {
            type: Boolean,
            trim: true,
            default: false,
        },
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('Flight', flightSchema)
