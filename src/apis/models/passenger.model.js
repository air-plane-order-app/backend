const mongoose = require('mongoose')
const Schema = mongoose.Schema

const passengerSchema = new mongoose.Schema(
    {
        firstName: {
            type: String,
            required: [true, 'Please enter your first name!'],
            trim: true,
        },
        lastName: {
            type: String,
            required: [true, 'Please enter your last name!'],
            trim: true,
        },
        address: {
            type: String,
            trim: true,
        },
        phoneNumber: {
            type: String,
            trim: true,
        },
        idNumber: {
            type: String,
            trim: true,
        },
        dob: {
            type: Date,
            trim: true,
        },
        pob: {
            type: String,
            trim: true,
        },
        avatar: {
            type: String,
            default: 'https://res.cloudinary.com/devatchannel/image/upload/v1602752402/avatar/avatar_cugq40.png',
        },
        user: { type: Schema.Types.ObjectId, ref: 'User' },
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('Passengers', passengerSchema)
