const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ticketSchema = new mongoose.Schema(
    {
        user: { type: Schema.Types.ObjectId, ref: 'User' },
        flight: { type: Schema.Types.ObjectId, ref: 'Flight' },
        totalPrice: { type: Number, trim: true },
        seat: { type: String, trim: true },
    },
    {
        timestamps: true,
    }
)

module.exports = mongoose.model('Ticket', ticketSchema)
