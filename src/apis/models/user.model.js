const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')

const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
        },
        email: {
            type: String,
            unique: true,
            trim: true,
        },
        phone: {
            type: String,
            trim: true,
        },
        address: {
            type: String,
            trim: true,
        },
        idNumber: {
            type: String,
            trim: true,
        },
        birthDate: {
            type: String,
            trim: true,
        },
        password: {
            type: String,
            required: [true, 'Please enter your password!'],
        },
        isVerified: {
            type: Boolean,
            default: false,
        },
        avatar: {
            type: String,
            trim: true,
        },
        role: { type: Schema.Types.ObjectId, ref: 'Role' },
    },
    {
        timestamps: true,
    }
)

userSchema.statics.isEmailTaken = async function (userEmail) {
    const user = await this.findOne({ email: userEmail })
    return !!user
}

userSchema.methods.isPasswordMatch = async function (password) {
    const user = this
    return bcrypt.compare(password, user.password)
}

userSchema.pre('save', async function (next) {
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 10)
    }
    next()
})

userSchema.pre('findOneAndUpdate', async function (next) {
    const data = this.getUpdate()
    const user = this
    if (!!data?.password) {
        this.update({}, { password: await bcrypt.hash(data.password, 10) })
    }
    next()
})

module.exports = mongoose.model('User', userSchema)
