const httpStatus = require('http-status')

const ApiError = require('../../utils/api-error')
const { Flight, Ticket } = require('../models')
const { findById } = require('../models/user.model')

const getAll = async (body) => {
    const { departureFromLocation, departureToLocation, departureStartTime, arrivalStartTime, type, totalSeat } = body

    return Flight.find({
        departureFromLocation,
        departureToLocation,
        departureStartTime,
        arrivalStartTime,
        type,
        isDeleted: false,
        // availableSeat: { $gte: totalSeat },
    })
}
const getAllPagination = async (body) => {
    const {
        departureFromLocation,
        departureToLocation,
        departureStartTime,
        arrivalStartTime,
        type,
        totalSeat,
        limitPage,
        skipPage,
    } = body

    return Flight.find({
        departureFromLocation,
        departureToLocation,
        departureStartTime,
        arrivalStartTime,
        type,
        isDeleted: false,
        // availableSeat: { $gte: totalSeat },
    })
        .limit(limitPage)
        .skip(skipPage)
}

const getAllNoPagination = async () => {
    return Flight.find({ isDeleted: false })
}

const createFlight = async (body) => {
    return Flight.create(body)
}

const deleteFlight = async (body) => {
    return Flight.findOneAndUpdate(
        { _id: body?.id },
        {
            isDeleted: true,
        }
    )
}

const createTicket = async ({ id, ticketId, totalPrice }) => {
    return await Ticket.create({ user: id, flight: ticketId, totalPrice: totalPrice })
}

const getTickets = async (id) => {
    return await Ticket.find({ user: id }).populate('flight')
}

const updateFlight = async ({ id, available }) => {
    const data = await Flight.findById(id)

    return await Flight.findOneAndUpdate(
        { _id: id },
        {
            availableSeat: data.availableSeat - available,
        }
    )
}

module.exports = {
    getAll,
    getAllPagination,
    getAllNoPagination,
    createFlight,
    deleteFlight,
    getTickets,
    createTicket,
    updateFlight,
}
