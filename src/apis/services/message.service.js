const httpStatus = require('http-status')

const ApiError = require('../../utils/api-error')
const { Message } = require('../models')

const createMessage = async (body) => {
    const { room, name } = body

    return Message.create({
        name,
        room,
    })
}

const getMessage = async () => {
    return Message.find({})
}

const deleteMessage = async (body) => {
    return Message.deleteOne({
        room: body?.room,
    })
}

module.exports = {
    createMessage,
    getMessage,
    deleteMessage,
}
