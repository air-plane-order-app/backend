const jwt = require('jsonwebtoken')
const moment = require('moment')

const { Token } = require('../models')

const { tokenTypes } = require('../../configs/tokens')
const { REFRESH_TOKEN_SECRET, ACCESS_TOKEN_SECRET, PASSPORT_JWT_ACCESS_EXPIRED, PASSPORT_JWT_REFRESH_EXPIRED } = process.env

const generateAuthTokens = async (user) => {
    const accessTokenExpires = moment(new Date()).add(PASSPORT_JWT_ACCESS_EXPIRED / 60, 'minutes')
    const accessToken = generateToken(user.id, accessTokenExpires, tokenTypes.ACCESS)

    const refreshTokenExpires = moment(new Date()).add(PASSPORT_JWT_REFRESH_EXPIRED / 60, 'minutes')
    const refreshToken = generateToken(user.id, refreshTokenExpires, tokenTypes.REFRESH)
    await saveToken(refreshToken, user.id, refreshTokenExpires, tokenTypes.REFRESH)

    return {
        access: {
            token: accessToken,
            expires: accessTokenExpires.toDate(),
        },
        refresh: {
            token: refreshToken,
            expires: refreshTokenExpires.toDate(),
        },
    }
}

const generateToken = (userId, expires, type, secret = ACCESS_TOKEN_SECRET) => {
    const payload = {
        sub: userId,
        iat: moment().unix(),
        exp: expires.unix(),
        type,
    }
    return jwt.sign(payload, secret)
}

const getTokenByRefresh = async (refreshToken) => {
    const refreshTokenDoc = await Token.findOne({
        token: refreshToken,
        type: tokenTypes.REFRESH,
    })
    return refreshTokenDoc
}

const logout = async (refreshToken) => {
    const refreshTokenDoc = await getTokenByRefresh(refreshToken)
    if (!refreshTokenDoc) {
        throw new ApiError(404, 'Not found')
    }
    return await refreshTokenDoc.remove()
}

const saveToken = async (token, userId, expires, type) => {
    const tokenDoc = await Token.create({
        token,
        user: userId,
        expires: expires.toDate(),
        type,
    })
    return tokenDoc
}

module.exports = {
    generateAuthTokens,
    generateToken,
    getTokenByRefresh,
    logout,
}
