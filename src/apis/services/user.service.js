const httpStatus = require('http-status')

const ApiError = require('../../utils/api-error')
const { User, Role } = require('../models')

const createUser = async (userBody) => {
    console.log('email', await User.isEmailTaken(userBody.email), userBody.email)
    if (await User.isEmailTaken(userBody.email)) {
        throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken')
    }

    let role
    if (!!userBody?.role && !(await Role.isRoleExist(userBody.role?.roleName))) {
        role = await Role.create(userBody.role)
    } else {
        role = await Role.findOne({ roleName: userBody?.role?.roleName })
    }

    if (!!role) {
        userBody.role = role._id
    }

    return User.create(userBody)
}

const loginUserWithEmailAndPassword = async (email, password) => {
    const user = await getUserByEmail(email)

    if (!user || !(await user.isPasswordMatch(password))) {
        throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password')
    }

    return user
}

const getUserByEmail = async (email) => {
    return await User.findOne({ email }).populate('role')
}

const getUserById = async (id) => {
    return await User.findById(id).populate('role')
}

const resetPassword = async (newPassword, id) => {
    return User.findOneAndUpdate(
        { _id: id },
        {
            password: newPassword,
        }
    )
}

const getAllUsers = async () => {
    return await User.find().select('-password').populate('role')
}

const deleteUser = async (id) => {
    return await User.findByIdAndDelete(id)
}

module.exports = {
    createUser,
    getUserByEmail,
    loginUserWithEmailAndPassword,
    getUserById,
    resetPassword,
    getAllUsers,
    deleteUser,
}
